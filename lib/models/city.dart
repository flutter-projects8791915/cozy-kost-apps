class City {
  int id;
  String name;
  String imageURL;
  bool isPopuler;

  City({this.id, this.imageURL, this.name, this.isPopuler = false});
}
