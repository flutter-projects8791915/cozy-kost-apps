import 'package:cozy_kost/models/city.dart';
import 'package:cozy_kost/models/space.dart';
import 'package:cozy_kost/models/tips.dart';
import 'package:cozy_kost/providers/space_provider.dart';
import 'package:cozy_kost/theme.dart';
import 'package:cozy_kost/widgets/bottom_navbar_item.dart';
import 'package:cozy_kost/widgets/city_card.dart';
import 'package:cozy_kost/widgets/space_card.dart';
import 'package:cozy_kost/widgets/tips_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var spaceProvider = Provider.of<SpaceProvider>(context);

    return Scaffold(
      backgroundColor: whiteColor,
      body: SafeArea(
        bottom: false,
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: defaultMargin,
            ),
            // NOTE: TITLE / HEADER
            Padding(
              padding: EdgeInsets.only(left: defaultMargin),
              child: Text(
                "Explore Now",
                style: blackTextStyle.copyWith(fontSize: 24),
              ),
            ),
            SizedBox(
              height: 2,
            ),
            Padding(
              padding: EdgeInsets.only(left: defaultMargin),
              child: Text(
                "Mencari kosan yang cozy",
                style: greyTextStyle.copyWith(fontSize: 16),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            // NOTE: POPULAR CITIES
            Padding(
              padding: EdgeInsets.only(left: defaultMargin),
              child: Text(
                "Popular Cities",
                style: regularTextStyle.copyWith(fontSize: 16),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Container(
              height: 150,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  SizedBox(
                    width: 24,
                  ),
                  CityCard(City(
                    id: 1,
                    name: 'Jakarta',
                    imageURL: 'assets/city1.png',
                  )),
                  SizedBox(
                    width: 20,
                  ),
                  CityCard(City(
                      id: 2,
                      name: 'Bandung',
                      imageURL: 'assets/city2.png',
                      isPopuler: true)),
                  SizedBox(
                    width: 20,
                  ),
                  CityCard(City(
                    id: 3,
                    name: 'Surabaya',
                    imageURL: 'assets/city3.png',
                  )),
                  SizedBox(
                    width: 24,
                  ),
                  CityCard(City(
                    id: 4,
                    name: 'Makassar',
                    imageURL: 'assets/city4.png',
                  )),
                  SizedBox(
                    width: 24,
                  ),
                  CityCard(City(
                    id: 5,
                    name: 'Yogyakarta',
                    imageURL: 'assets/city5.png',
                  )),
                  SizedBox(
                    width: 24,
                  ),
                  CityCard(City(
                    id: 6,
                    name: 'Malang',
                    imageURL: 'assets/city6.png',
                  )),
                  SizedBox(
                    width: 24,
                  )
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            // NOTE: RECOMENDED SPACE
            Padding(
              padding: EdgeInsets.only(left: defaultMargin),
              child: Text(
                "Recomended Space",
                style: regularTextStyle.copyWith(fontSize: 16),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Padding(
              padding: EdgeInsets.only(left: defaultMargin),
              child: FutureBuilder(
                future: spaceProvider.getRecommendedSpaces(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<Space> data = snapshot.data;

                    int index = 0;

                    return Column(
                      children: data.map((item) {
                        index++;
                        return Container(
                          margin: EdgeInsets.only(
                            top: index == 1 ? 0 : 30,
                          ),
                          child: SpaceCard(item),
                        );
                      }).toList(),
                    );
                  }

                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              ),
            ),
            SizedBox(
              height: 30,
            ),
            // NOTE: TIPS & GUIDANCE
            Padding(
              padding: EdgeInsets.only(left: defaultMargin),
              child: Text(
                "Tips & Guidance",
                style: regularTextStyle.copyWith(fontSize: 16),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Padding(
              padding: EdgeInsets.only(left: defaultMargin),
              child: Column(
                children: [
                  TipsCard(Tips(
                    id: 1,
                    title: 'City Guidelines',
                    imageUrl: 'assets/tips1.png',
                    updatedAt: '20 Apr',
                  )),
                  SizedBox(
                    height: 20,
                  ),
                  TipsCard(Tips(
                    id: 2,
                    title: 'Jakarta Fairship',
                    imageUrl: 'assets/tips2.png',
                    updatedAt: '11 Dec',
                  )),
                ],
              ),
            ),
            SizedBox(
              height: 80 + defaultMargin,
            ),
          ],
        ),
      ),
      floatingActionButton: Container(
        height: 65,
        width: MediaQuery.of(context).size.width - 2 * defaultMargin,
        margin: EdgeInsets.only(left: defaultMargin, right: defaultMargin),
        decoration: BoxDecoration(
          color: Color(0xfff6f7f8),
          borderRadius: BorderRadius.circular(23),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            BottomNavbarItem(
              imageUrl: 'assets/icon_home.png',
              isActive: true,
            ),
            BottomNavbarItem(
              imageUrl: 'assets/icon_email.png',
              isActive: false,
            ),
            BottomNavbarItem(
              imageUrl: 'assets/icon_card.png',
              isActive: false,
            ),
            BottomNavbarItem(
              imageUrl: 'assets/icon_love.png',
              isActive: false,
            )
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
